import logo from './logo.svg';
import './App.css';
import PaymentComponent from './components/payment';

function App() {
  return (
    <div className="App">
      <PaymentComponent />
    </div>
  );
}

export default App;
